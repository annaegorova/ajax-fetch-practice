import { SectionCreator } from './components/SectionCreator.js';
import '../styles/style.css';

document.addEventListener('DOMContentLoaded', () => {
  SectionCreator.create('community').render();
  SectionCreator.create('standard').render();
});
