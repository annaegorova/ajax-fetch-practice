export class Section {
  constructor(renderSection, className) {
    this.renderSection = renderSection;
    this.className = className;
  }

  render() {
    this.renderSection();
  }

  remove() {
    document.getElementsByClassName(this.className)[0].remove();
  }
}
