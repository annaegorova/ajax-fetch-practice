import {
  JOIN_PROGRAM_HEADER,
  JOIN_PROGRAM_HEADER_ADVANCED,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED
} from '../constants/content-constants';
import { JoinUsSection } from './JoinUsSection';
import { CommunitySection } from './CommunitySection';

export class SectionCreator {
  static create(type) {
    switch (type) {
      case 'advanced':
        return new JoinUsSection(
            JOIN_PROGRAM_HEADER_ADVANCED,
            JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED
        );
      case 'standard':
        return new JoinUsSection(
            JOIN_PROGRAM_HEADER,
            JOIN_PROGRAM_BUTTON_SUBSCRIBE
        );
      case 'community':
        return new CommunitySection();
    }
  }
}
